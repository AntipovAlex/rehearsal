package com.antipov.brutaltech.services;

import com.antipov.brutaltech.utils.managers.DeviceTokenManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Antipov on 10.05.17.
 */

public class FirebaseInstanceService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        saveToken(refreshedToken);
    }

    private void saveToken(String refreshedToken) {
        DeviceTokenManager tokenManager = new DeviceTokenManager(getApplicationContext());
        tokenManager.setDeviceToken(refreshedToken);
    }


}
