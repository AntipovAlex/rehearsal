package com.antipov.brutaltech.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.antipov.brutaltech.R;
import com.antipov.brutaltech.ui.activity.bookings.BookingsActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

/**
 * Created by Antipov on 09.05.17.
 */

public class FirebasePushService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        sendNotification(remoteMessage.getData());

    }
    private void sendNotification(Map<String, String> messageBody) {
        Intent notificationIntent = new Intent(getApplicationContext(), BookingsActivity.class);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(getApplicationContext(), 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);


        NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                .setAutoCancel(true)
                .setContentTitle(messageBody.get("title"))
                .setContentText(messageBody.get("content"))
                .setContentIntent(resultPendingIntent)
                .setSmallIcon(R.drawable.ic_launcher);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        notificationManager.notify(0, notification.build());


    }
}
