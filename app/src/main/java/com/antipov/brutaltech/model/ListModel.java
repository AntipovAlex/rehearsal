package com.antipov.brutaltech.model;

/**
 * Created by antipov on 14.02.17.
 */

public class ListModel {
    private String time;
    private String state;
    private int stateColor;
    private boolean checkboxEnabled;
    private String checkboxText;

    public ListModel(String time, String checkboxText, boolean checkboxEnabled, int stateColor, String state) {
        this.time = time;
        this.checkboxText = checkboxText;
        this.checkboxEnabled = checkboxEnabled;
        this.stateColor = stateColor;
        this.state = state;
    }



    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getStateColor() {
        return stateColor;
    }

    public void setStateColor(int stateColor) {
        this.stateColor = stateColor;
    }

    public String getCheckboxText() {
        return checkboxText;
    }

    public void setCheckboxText(String checkboxText) {
        this.checkboxText = checkboxText;
    }

    public boolean isCheckboxEnabled() {
        return checkboxEnabled;
    }

    public void setCheckboxEnabled(boolean checkboxEnabled) {
        this.checkboxEnabled = checkboxEnabled;
    }
}
