package com.antipov.brutaltech.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BookingModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("telephone")
    @Expose
    private String telephone;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("admin_comment")
    @Expose
    private String adminComment;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("booked")
    @Expose
    private boolean booked;

    public String getAdminComment() {
        return adminComment;
    }

    public void setAdminComment(String adminComment) {
        this.adminComment = adminComment;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public boolean isWaiting() {
        return !booked;
    }

    public void setBooked(boolean booked) {
        this.booked = booked;
    }
}
