package com.antipov.brutaltech.model.response;


import com.antipov.brutaltech.model.BookingModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Antipov on 29.04.17.
 */

public class BookingResponseModel {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("data")
    @Expose
    private List<BookingModel> data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<BookingModel> getData() {
        return data;
    }

    public void setData(List<BookingModel> data) {
        this.data = data;
    }

    public boolean isResponseSuccessful() {
        return status;
    }
}
