package com.antipov.brutaltech.ui.activity.base;

import com.antipov.brutaltech.model.UserModel;

/**
 * Created by Antipov on 20.08.2017.
 */

public interface BaseView {

    void checkAuth();

    void ifAuthenticated(UserModel model);

    void ifNotAuthenticated();

    void onNoInternet();
}
