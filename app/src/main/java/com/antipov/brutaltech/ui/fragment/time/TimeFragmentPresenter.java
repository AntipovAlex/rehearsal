package com.antipov.brutaltech.ui.fragment.time;

import android.content.Context;

/**
 * Created by Antipov on 27.08.2017.
 */

interface TimeFragmentPresenter {
    void book(Context context, String token, int year, int month, int day, String times, String name,
              String tel, String comment, int type);

    void getTime(Context context, int year, int month, int day);

    void sendNotifyRequest(Context context,  String name, int year, int month, int day, String time, String phone);

    void onDestroy();
}
