package com.antipov.brutaltech.ui.activity.rules;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.antipov.brutaltech.R;
import com.antipov.brutaltech.model.UserModel;
import com.antipov.brutaltech.ui.activity.base.BaseActivity;
import com.antipov.brutaltech.utils.Constants;

public class WebViewActivity extends BaseActivity {
    private WebView webView;
    private Toolbar toolbar;
    private String rulesUrl;
    private String title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        rulesUrl = getIntent().getExtras().getString(Constants.Intent.URL);
        title = getIntent().getExtras().getString(Constants.Intent.TITLE);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_rules;
    }

    @Override
    protected void initViews() {
        webView = findViewById(R.id.webview);
        toolbar = findViewById(R.id.toolbar);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setUserAgentString("Mozilla/5.0 (Linux; Android 4.1.1; Galaxy Nexus Build/JRO03C) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19");
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        webView.loadUrl(rulesUrl);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void initListeners() {

    }

    @Override
    protected void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setSubtitle(title);
    }

    @Override
    protected void initPresenter() {

    }

    @Override
    public void ifAuthenticated(UserModel model) {

    }

    @Override
    public void ifNotAuthenticated() {

    }

    @Override
    public void onNoInternet() {

    }
}
