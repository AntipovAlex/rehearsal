package com.antipov.brutaltech.ui.activity.settings;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.antipov.brutaltech.R;
import com.antipov.brutaltech.model.UserModel;
import com.antipov.brutaltech.ui.activity.base.BaseActivity;
import com.antipov.brutaltech.ui.fragment.settings.SettingsFragment;


public class SettingsActivity extends BaseActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(R.id.container, SettingsFragment.newInstance()).commit();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_settings;
    }

    @Override
    protected void initViews() {
        toolbar =  findViewById(R.id.toolbar);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void initListeners() {

    }

    @Override
    protected void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setSubtitle(R.string.nav_settings);
    }

    @Override
    protected void initPresenter() {

    }

    @Override
    public void ifAuthenticated(UserModel model) {

    }

    @Override
    public void ifNotAuthenticated() {

    }

    @Override
    public void onNoInternet() {

    }
}
