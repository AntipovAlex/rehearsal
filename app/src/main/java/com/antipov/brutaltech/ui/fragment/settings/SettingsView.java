package com.antipov.brutaltech.ui.fragment.settings;

import com.antipov.brutaltech.model.response.ActionResponseModel;

/**
 * Created by Antipov on 17.09.2017.
 */

public interface SettingsView {
    interface OnUpdateUser{
        void onUpdateUserSuccess(ActionResponseModel model);

        void onUpdateUserFailure(String error);
    }

    void initViews();

    void initListeners();

    void initPresenter();

    void onNoInternet();
}
