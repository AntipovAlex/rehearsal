package com.antipov.brutaltech.ui.activity.main;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.antipov.brutaltech.R;
import com.antipov.brutaltech.interfaces.OnBookedSuccessfully;
import com.antipov.brutaltech.model.UserModel;
import com.antipov.brutaltech.model.response.ActionResponseModel;
import com.antipov.brutaltech.ui.activity.about.AboutActivity;
import com.antipov.brutaltech.ui.activity.bookings.BookingsActivity;
import com.antipov.brutaltech.ui.activity.rules.WebViewActivity;
import com.antipov.brutaltech.ui.activity.settings.SettingsActivity;
import com.antipov.brutaltech.ui.activity.base.BaseActivity;
import com.antipov.brutaltech.ui.activity.contact.ContactActivity;
import com.antipov.brutaltech.ui.fragment.calendar.CalendarFragment;
import com.antipov.brutaltech.utils.Constants;
import com.antipov.brutaltech.utils.DialogUtils;
import com.antipov.brutaltech.utils.FragmentUtils;
import com.antipov.brutaltech.utils.managers.DeviceTokenManager;
import com.antipov.brutaltech.utils.managers.SystemManager;
import com.antipov.brutaltech.utils.managers.UserInfoManager;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.antipov.brutaltech.App.sessionManager;

public class MainActivity extends BaseActivity implements View.OnClickListener,
        NavigationView.OnNavigationItemSelectedListener, MainView.OnSignin, MainView.OnRegistration, OnBookedSuccessfully {
    UserModel model;
    private Boolean isLoggedIn;
    private Toolbar toolbar;
    private DrawerLayout mDrawer;
    private ActionBarDrawerToggle drawerToggle;
    private NavigationView mNavigationView;
    private Button mAuthButton;
    private Button mRegisterButton;
    private View headerLayout;
    private RelativeLayout mAnonContainer;
    private RelativeLayout mUserContainer;
    private MaterialDialog dialogRegister;
    private MaterialDialog dialogAuth;
    private MaterialDialog.SingleButtonCallback authListener;
    private MaterialDialog.SingleButtonCallback registerListener;
    private DeviceTokenManager tokenManager;
    private UserInfoManager userManager;
    private MainPresenterImpl mPresenter;
    private static final int SETTINGS = 1;
    private TextView mNotifs;
    private SystemManager systemManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkAuth();
        tokenManager = new DeviceTokenManager(context);
        userManager = new UserInfoManager(context);
        systemManager = new SystemManager(context);
        if (systemManager.isFisrstTime()){
            showHelp();
            systemManager.setBeforeLaunched();
        }
    }

    private void showHelp() {
        Intent i = new Intent(this, AboutActivity.class);
        startActivity(i);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initViews(){
        toolbar =  findViewById(R.id.toolbar);
        mNavigationView = findViewById(R.id.nav_view);
        mDrawer = findViewById(R.id.drawer_layout);
        headerLayout = mNavigationView.getHeaderView(0);
        mAuthButton = headerLayout.findViewById(R.id.btn_register);
        mRegisterButton = headerLayout.findViewById(R.id.btn_signin);
        mAnonContainer = headerLayout.findViewById(R.id.rv_anon_container);
        mUserContainer = headerLayout.findViewById(R.id.rv_user_container);
        mNotifs = (TextView) mNavigationView.getMenu().findItem(R.id.nav_my_bookings).getActionView();
        FragmentUtils.replaceFragment(getSupportFragmentManager(), CalendarFragment.newInstance(model));
    }

    @Override
    protected void initToolbar(){
        setSupportActionBar(toolbar);
        drawerToggle = setupDrawerToggle();
        mDrawer.addDrawerListener(drawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        drawerToggle.syncState();
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.drawer_open,  R.string.drawer_close);
    }

    @Override
    protected void initListeners(){
        mAuthButton.setOnClickListener(this);
        mRegisterButton.setOnClickListener(this);
        mNavigationView.setNavigationItemSelectedListener(this);
        authListener = new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                showProgress();
                mPresenter.signin(context, String.valueOf(((TextView)dialog.findViewById(R.id.tv_login)).getText().toString().trim()),
                        String.valueOf(((TextView)dialog.findViewById(R.id.tv_password)).getText()), tokenManager.getDeviceToken());
            }
        };

        registerListener = new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                if (validateFields(
                        (TextView)dialog.findViewById(R.id.tv_login),
                        (TextView)dialog.findViewById(R.id.tv_email),
                        (TextView)dialog.findViewById(R.id.tv_first_name),
                        (TextView)dialog.findViewById(R.id.tv_secondname),
                        (TextView)dialog.findViewById(R.id.tv_phone),
                        (TextView)dialog.findViewById(R.id.tv_password),
                        (TextView)dialog.findViewById(R.id.tv_confirm_pass)
                )){
                    showProgress();
                    hideSoftKeyboard();
                    mPresenter.register(context,
                            ((TextView)dialog.findViewById(R.id.tv_login)).getText().toString().trim(),
                            ((TextView)dialog.findViewById(R.id.tv_email)).getText().toString().trim(),
                            ((TextView)dialog.findViewById(R.id.tv_password)).getText().toString().trim(),
                            ((TextView)dialog.findViewById(R.id.tv_first_name)).getText().toString().trim(),
                            ((TextView)dialog.findViewById(R.id.tv_secondname)).getText().toString().trim(),
                            ((TextView)dialog.findViewById(R.id.tv_phone)).getText().toString().trim()
                    );
                }
            }
        };
    }

    private boolean validateFields(TextView login, TextView email, TextView first, TextView second, TextView phone, TextView password, TextView repeatPassword) {
        Matcher m = Pattern.compile(Constants.REG_EXP_USERNAME).matcher(login.getText().toString().trim());
        if (!m.matches()) {
            login.setError(getString(R.string.bad_username));
            return false;
        }
        m = Pattern.compile(Constants.REG_EXP_EMAIL).matcher(email.getText().toString().trim());
        if (!m.matches()) {
            email.setError(getString(R.string.bad_email));
            return false;
        }
        m = Pattern.compile(Constants.REG_EXP_NAME).matcher(first.getText().toString().trim());
        if (!m.matches()) {
            first.setError(getString(R.string.bad_name));
            return false;
        }
        m = Pattern.compile(Constants.REG_EXP_NAME).matcher(second.getText().toString().trim());
        if (!m.matches()) {
            second.setError(getString(R.string.bad_name));
            return false;
        }
        m = Pattern.compile(Constants.REG_EXP_PHONE).matcher(phone.getText().toString().trim());
        if (!m.matches()) {
            phone.setError(getString(R.string.bad_phone));
            return false;
        }
        m = Pattern.compile(Constants.REG_EXP_PASSWORD).matcher(password.getText().toString().trim());
        if (!m.matches()) {
            password.setError(getString(R.string.bad_password));
            return false;
        }
        if (!password.getText().toString().equals(repeatPassword.getText().toString().trim())){
            repeatPassword.setError(getString(R.string.passwords_no_matches));
            return false;
        }
        return true;
    }

    @Override
    protected void initPresenter() {
        mPresenter = new MainPresenterImpl(this);
    }

    private void initDrawer(Boolean isLoggedIn){
        if (isLoggedIn){ //making drawer for authenticated user
            mNavigationView.getMenu().setGroupEnabled(R.id.nav_general, true);
            mAnonContainer.setVisibility(View.GONE);
            mUserContainer.setVisibility(View.VISIBLE);
            updateNavHeader();
            if (model.getNotificationse() != 0) {
                updateNotifications(model.getNotificationse());
            }
        } else {//making drawer for NON authenticated user
            mNavigationView.getMenu().setGroupEnabled(R.id.nav_general, false);
            mAnonContainer.setVisibility(View.VISIBLE);
            mUserContainer.setVisibility(View.GONE);
        }
    }

    private void updateNotifications(int notifications) {
            mNotifs.setGravity(Gravity.CENTER_VERTICAL);
            mNotifs.setTypeface(null, Typeface.BOLD);
            mNotifs.setTextColor(getResources().getColor(R.color.booked));
            mNotifs.setText(String.valueOf(notifications));
    }

    @Override
    public void onBookedSuccessfully(int count) {
        if (sessionManager.isLoggedIn()){
            updateNotifications(model.getNotificationse()+count);
        }
    }

    @Override
    public void onSigninSuccess(UserModel model) {
        hideProgress();
        dialogAuth.dismiss();
        userManager.saveUser(model.getEmail(), model.getFirstName(), model.getLastName(), model.getTelephone());
        sessionManager.logInUser(model.getToken());
        ifAuthenticated(model);
    }

    @Override
    public void onSigninFailure(String error) {
        hideProgress();
        ((TextView)dialogAuth.findViewById(R.id.tv_password)).setError(getString(R.string.wrong_password));
        ((TextView)dialogAuth.findViewById(R.id.tv_password)).setText("");
    }

    @Override
    public void onRegistrationSuccess(ActionResponseModel model) {
        hideProgress();
        dialogRegister.dismiss();
        mDrawer.closeDrawer(Gravity.LEFT, true);
        DialogUtils.showSnackbar(this, getString(R.string.register_succes));
    }

    @Override
    public void onRegistrationFailure(String errors) {
        hideProgress();
        DialogUtils.showSnackbar(this, getString(R.string.register_error)+errors);
    }

    // showing authed layout
    @Override
    public void ifAuthenticated(UserModel model) {
        this.isLoggedIn = true;
        this.model = model;
        userManager.saveUser(
                model.getEmail(),
                model.getFirstName(),
                model.getLastName(),
                model.getTelephone()
        );
        initDrawer(isLoggedIn);
        hideProgress();
    }

    // showing not authed layout
    @Override
    public void ifNotAuthenticated() {
        this.isLoggedIn = false;
        initDrawer(isLoggedIn);
        hideProgress();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_register:
                if (dialogRegister == null){
                    dialogRegister = DialogUtils.createCustomDialog(this, getString(R.string.register_title),
                            R.layout.dialog_register, false, registerListener, getString(R.string.cancel), getString(R.string.ok)).show();
                } else {
                    dialogRegister.show();
                }

                break;
            case R.id.btn_signin:
                if (dialogAuth == null){
                    dialogAuth = DialogUtils.createCustomDialog(this, getString(R.string.auth_title),
                            R.layout.dialog_auth, false, authListener, getString(R.string.cancel), getString(R.string.ok)).show();
                } else {
                    dialogAuth.show();
                }
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent i;
        switch (item.getItemId()){
            case R.id.nav_my_bookings:
                i = new Intent(this, BookingsActivity.class);
                startActivity(i);
                break;
            case R.id.nav_help:
                showHelp();
                break;
            case R.id.nav_rules:
                i = new Intent(this, WebViewActivity.class);
                i.putExtra(Constants.Intent.URL, Constants.RULES_URL);
                i.putExtra(Constants.Intent.TITLE, getString(R.string.nav_rules));
                startActivity(i);
                break;
            case R.id.nav_site:
                Intent browserIntent =
                        new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.SITE_URL));
                startActivity(browserIntent);
                break;
            case R.id.nav_contacts:
                i = new Intent(this, ContactActivity.class);
                startActivity(i);
                break;
            case R.id.nav_settings:
                i = new Intent(this, SettingsActivity.class);
                startActivityForResult(i, SETTINGS);
                break;
            case R.id.nav_logout:
                sessionManager.removeSession();
                ifNotAuthenticated();
                break;
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SETTINGS & resultCode == RESULT_OK){
            updateNavHeader();
            DialogUtils.showSnackbar(this, getString(R.string.user_updated));
        }
    }

    private void updateNavHeader() {
        ((TextView)mUserContainer.findViewById(R.id.tv_name)).setText(userManager.getFname()+" "+userManager.getSname());
        ((TextView)mUserContainer.findViewById(R.id.tv_email)).setText(userManager.getEmail());
        ((TextView)mUserContainer.findViewById(R.id.tv_phone)).setText(userManager.getPhone());
    }

    @Override
    public void onNoInternet() {
        hideProgress();
        DialogUtils.showSnackbar(this, getString(R.string.no_internet_connection));
    }

    @Override
    public void onBackPressed() {
        // back navigation
        int count = getFragmentManager().getBackStackEntryCount();
        if (count == 0) {
            super.onBackPressed();
        } else {
            getFragmentManager().popBackStack();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }
}
