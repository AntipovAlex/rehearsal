package com.antipov.brutaltech.ui.fragment.my_booking;

import android.content.Context;

/**
 * Created by Antipov on 12.09.2017.
 */

public interface MyBookingsFragmentPresenter {
    void getBookings(Context context, String token, String key, int page);

    void onDestroy();
}
