package com.antipov.brutaltech.ui.fragment.my_booking;

import android.content.Context;

import com.antipov.brutaltech.model.response.BookingResponseModel;

/**
 * Created by Antipov on 12.09.2017.
 */

public class MyBookingsFragmentPresenterImpl implements MyBookingsFragmentPresenter, MyBookingsFragmentInteractor.OnGetBookings, MyBookingsFragmentInteractor.OnNoInternet {

    private final MyBookingsFragmentInteractorImpl mInteractor;
    private MyBookingsFragment mView;

    public MyBookingsFragmentPresenterImpl(MyBookingsFragment view) {
        mView = view;
        mInteractor = new MyBookingsFragmentInteractorImpl();
    }

    @Override
    public void getBookings(Context context, String token, String key, int page) {
        mInteractor.getBookings(context, token, key, page, this, this);
    }

    @Override
    public void onGetBookingsSuccess(BookingResponseModel model) {
        if (mView != null){
            mView.onGetBookingsSuccess(model);
        }
    }

    @Override
    public void onGetBookingsFailure(String error) {
        if (mView != null){
            mView.onGetBookingsFailure(error);
        }
    }

    @Override
    public void onNoInternet() {
        if (mView != null){
            mView.onNoInternet();
        }
    }

    @Override
    public void onDestroy() {
        if (mView != null){
            mView = null;
        }
    }
}
