package com.antipov.brutaltech.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.antipov.brutaltech.R;
import com.antipov.brutaltech.model.BookingModel;
import com.antipov.brutaltech.model.response.BookingResponseModel;
import com.antipov.brutaltech.utils.Utils;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Antipov on 13.09.2017.
 */

public class BookingsAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final ArrayList<Data> mData;
    private final Context context;
    private final int SECTION = 0;
    private final int CARD = 1;

    public BookingsAdapter(Context ctx) {
        mData = new ArrayList<>();
        context = ctx;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == SECTION){
            // inflating section
            return new SectionVH(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recycler_section, parent, false));
        } else {
            // inflating card
            return new CardVH(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recycler_item_bookings, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()){
            case CARD:
                CardVH card = (CardVH) holder;
                card.date.setText(mData.get(position).getDate());
                card.time.setText(mData.get(position).getTime());
                if (mData.get(position).isApproved()){
                    card.state.setText(context.getString(R.string.booking_state_booked));
                    card.state.setTextColor(context.getResources().getColor(R.color.not_booked));
                } else {
                    card.state.setText(context.getString(R.string.booking_state_waiting));
                    card.state.setTextColor(context.getResources().getColor(R.color.waiting));
                }
                if (mData.get(position).getAdminComment() == null){
                    card.adminComment.setVisibility(View.GONE);
                } else {
                    card.adminComment.setVisibility(View.VISIBLE);
                    card.adminComment.setText(context.getString(R.string.bookings_admin_comment_format, mData.get(position).getAdminComment()));
                }
                if (mData.get(position).getComment().equals("")){
                    card.comment.setVisibility(View.GONE);
                } else {
                    card.comment.setVisibility(View.VISIBLE);
                    card.comment.setText(context.getString(R.string.bookings_comment_format, mData.get(position).getComment()));
                }
                break;
            case SECTION:
                SectionVH section = (SectionVH) holder;
                section.date.setText(mData.get(position).getDate());
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mData.get(position).getType() == CARD) {
            return CARD;
        } else {
            return SECTION;
        }
    }

    public void add(BookingResponseModel model){
        mData.addAll(convert(model));
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class CardVH extends RecyclerView.ViewHolder {
        TextView state;
        TextView date;
        TextView time;
        TextView comment;
        TextView adminComment;

        public CardVH(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.mybooking_date_text);
            time = itemView.findViewById(R.id.mybooking_time_text);
            state = itemView.findViewById(R.id.mybooking_state);
            comment = itemView.findViewById(R.id.mybooking_comment_text);
            adminComment = itemView.findViewById(R.id.mybooking_admin_comment_text);
        }
    }

    public class SectionVH extends RecyclerView.ViewHolder {
        TextView date;

        public SectionVH(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.section_tex);
        }
    }

    private ArrayList<Data> convert(BookingResponseModel model){
        List<BookingModel> items = model.getData();
        Set<String> headers = new LinkedHashSet<>();
        ArrayList<Data> data = new ArrayList<>();

        // get unique dates
        for (BookingModel i : model.getData()){
            headers.add(Utils.parseDate(i.getTime()));
        }

        // forming hash map with times
        for (String header: headers){
            data.add(new Data(SECTION, header));
            for (BookingModel item: items){
                if (Utils.parseDate(item.getTime()).equals(header)){
                    data.add(new Data(
                            CARD,
                            Utils.parseDate(item.getTime()),
                            Utils.parseTime(item.getTime()),
                            item.getComment(),
                            item.getAdminComment(),
                            !item.isWaiting()
                    ));
                }
            }
        }
        return data;
    }

    private class Data{
        private int type;
        private String date;
        private String time;
        private String comment;
        private String adminComment;
        private boolean approved;

        public Data(int type, String date) {
            this.type = type;
            this.date = date;
        }

        public Data(int type, String date, String time, String comment, String adminComment, boolean approved) {
            this.type = type;
            this.date = date;
            this.time = time;
            this.comment = comment;
            this.adminComment = adminComment;
            this.approved = approved;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getAdminComment() {
            return adminComment;
        }

        public void setAdminComment(String adminComment) {
            this.adminComment = adminComment;
        }

        public boolean isApproved() {
            return approved;
        }

        public void setApproved(boolean approved) {
            this.approved = approved;
        }
    }
}
