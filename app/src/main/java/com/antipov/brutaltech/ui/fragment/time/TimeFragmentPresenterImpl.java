package com.antipov.brutaltech.ui.fragment.time;

import android.content.Context;

import com.antipov.brutaltech.model.response.ActionResponseModel;
import com.antipov.brutaltech.model.response.BookingResponseModel;

/**
 * Created by Antipov on 27.08.2017.
 */

public class TimeFragmentPresenterImpl implements TimeFragmentPresenter, TimeFragmentInteractor.OnGetTime, TimeFragmentInteractor.OnBookTime,
        TimeFragmentInteractor.OnNoInternet, TimeFragmentInteractor.OnSendNotifyRequest {

    private TimeFragmentInteractorImpl mInteractor;
    private TimeFragment mView;

    public TimeFragmentPresenterImpl(TimeFragment timeFragment) {
        mView = timeFragment;
        mInteractor = new TimeFragmentInteractorImpl();
    }

    @Override
    public void book(Context context, String token, int year, int month, int day, String times, String name, String tel, String comment, int type) {
        mInteractor.book(context, token, year, month, day, times, name, tel, comment, type, this, this);
    }

    @Override
    public void getTime(Context context, int year, int month, int day) {
        mInteractor.getTime(context, year, month, day, this, this);
    }

    @Override
    public void sendNotifyRequest(Context context, String name, int year, int month,
                                  int day, String time, String phone) {
        mInteractor.sendNotifyRequest(context, name, year, month, day, time, phone, this, this);
    }

    @Override
    public void onGetTimeSuccess(BookingResponseModel model) {
        if (mView != null){
            mView.onGetTimeSuccess(model);
        }
    }

    @Override
    public void onGetTimeFailure(String error) {
        if (mView != null){
            mView.onGetTimeFailure(error);
        }
    }


    @Override
    public void onBookTimeSuccess(ActionResponseModel model) {
        if (mView != null){
            mView.onBookTimeSuccess(model);
        }
    }

    @Override
    public void onBookTimeFailure(String error) {
        if (mView != null){
            mView.onBookTimeFailure(error);
        }
    }

    @Override
    public void onSendNotifyRequestSuccess(ActionResponseModel model) {
        if (mView != null){
            mView.onSendNotifyRequestSuccess(model);
        }
    }

    @Override
    public void onSendNotifyRequestFailure(String error) {
        if (mView != null){
            mView.onSendNotifyRequestFailure(error);
        }
    }

    @Override
    public void onNoInternet() {
        if (mView != null){
            mView.onNoInternet();
        }
    }

    @Override
    public void onDestroy() {
        if (mView != null){
            mView = null;
        }
    }
}
