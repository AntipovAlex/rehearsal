package com.antipov.brutaltech.ui.activity.main;

import android.content.Context;

import com.antipov.brutaltech.model.UserModel;
import com.antipov.brutaltech.model.response.ActionResponseModel;

/**
 * Created by Antipov on 04.09.17.
 */

interface MainInteractor {
    void signin(Context context, String login, String password, String deviceToken, OnNoInternet onNoInternet, OnSignin onSignin);

    void registration(Context context, String username, String email, String password, String firstname, String lastname, String phone, OnRegistration onRegistration, OnNoInternet onNoInternet);

    interface OnRegistration{
        void onRegistrationSuccess(ActionResponseModel model);

        void onRegistrationFailure(String errors);
    }

    interface OnSignin{
        void onSigninSuccess(UserModel model);

        void onSigninFailure(String error);
    }

    interface OnNoInternet{
        void onNoInternet();
    }

}
