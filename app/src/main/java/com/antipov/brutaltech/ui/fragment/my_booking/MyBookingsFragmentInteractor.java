package com.antipov.brutaltech.ui.fragment.my_booking;

import android.content.Context;
import com.antipov.brutaltech.model.response.BookingResponseModel;


/**
 * Created by Antipov on 12.09.2017.
 */

public interface MyBookingsFragmentInteractor {
    void getBookings(Context context, String token, String key, int page, OnGetBookings onGetBookings,
                     OnNoInternet onNoInternet);

    interface OnGetBookings{
        void onGetBookingsSuccess(BookingResponseModel model);

        void onGetBookingsFailure(String error);
    }

    interface OnNoInternet{
        void onNoInternet();
    }
}
