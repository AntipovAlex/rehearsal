package com.antipov.brutaltech.ui.fragment.about;

import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.antipov.brutaltech.R;
import com.antipov.brutaltech.ui.fragment.base.BaseFragment;

/**
 * Created by Antipov on 19.09.17.
 */

public class AboutFragment extends BaseFragment {

    private int position;
    private Button mUnderstand;
    private int lastPage;
    private TextView mAbout;
    private TextView mTitle;

    public static AboutFragment newInstance(int position, int numPages){
        Bundle args = new Bundle();
        AboutFragment fragment = new AboutFragment();
        args.putSerializable("pos", position);
        args.putSerializable("num", numPages);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_about;
    }

    @Override
    public void getArgsFromBundle() {
        position = getArguments().getInt("pos");
        lastPage = getArguments().getInt("num");
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initViews() {
        mAbout = mView.findViewById(R.id.tv_about);
        mTitle = mView.findViewById(R.id.tv_title);
        switch (position){
            case 0:
                mAbout.setText(R.string.about1);
                mTitle.setText(R.string.aboutHeader1);
                break;
            case 1:
                mAbout.setText(R.string.about2);
                mTitle.setText(R.string.aboutHeader2);
                break;
            case 2:
                mAbout.setText(R.string.about3);
                mTitle.setText(R.string.aboutHeader3);
                break;
            case 3:
                mAbout.setText(R.string.about4);
                mTitle.setText(R.string.aboutHeader4);
                break;
            case 4:
                mAbout.setText(R.string.about5);
                mTitle.setText(R.string.aboutHeader5);
                break;
            case 5:
                mAbout.setText(R.string.about6);
                mTitle.setText(R.string.aboutHeader6);
                break;
        }
        mUnderstand = mView.findViewById(R.id.btn_understand);
        if (position+1 == lastPage){
            mUnderstand.setVisibility(View.VISIBLE);
            mUnderstand.setPaintFlags(mUnderstand.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        }
    }

    @Override
    public void initListener() {
        if (position+1 == lastPage){
            mUnderstand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getActivity().finish();
                }
            });
        }
    }

    @Override
    public void onNoInternet() {

    }
}
