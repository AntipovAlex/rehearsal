package com.antipov.brutaltech.ui.activity.base;


import android.content.Context;

import com.antipov.brutaltech.API.BookingAPI;
import com.antipov.brutaltech.R;
import com.antipov.brutaltech.model.response.UserResponseModel;
import com.antipov.brutaltech.utils.Constants;
import com.antipov.brutaltech.utils.RetrofitUtils;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Antipov on 21.08.2017.
 */

public class BaseInteractorImp implements BaseInteractor {
    @Override
    public void checkAuth(final Context context, String token, final OnCheckAuth onCheckAuthListener, final OnNoInternet onNoInternetListener) {
        Observable<UserResponseModel> call = RetrofitUtils.getApiBookingApiClient().
                create(BookingAPI.class).getUserData(token);
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(Constants.RETRY_COUNT)
                .subscribe(new Subscriber<UserResponseModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e.toString().contains(context.getString(R.string.err_no_internet))) {
                            onNoInternetListener.onNoInternet();
                        } else {
                            onCheckAuthListener.onCheckAuthFailure(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UserResponseModel model) {
                        if (model != null && model.isResponseSuccessful()){
                            onCheckAuthListener.onCheckAuthSuccess(model.getData());
                        } else {
                            onCheckAuthListener.onCheckAuthFailure("err");
                        }
                    }
                });
    }
}
