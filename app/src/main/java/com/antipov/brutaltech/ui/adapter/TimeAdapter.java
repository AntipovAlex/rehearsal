package com.antipov.brutaltech.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.antipov.brutaltech.R;
import com.antipov.brutaltech.interfaces.OnNotifyClick;
import com.antipov.brutaltech.model.BookingModel;
import com.antipov.brutaltech.utils.Constants;
import com.antipov.brutaltech.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antipov on 27.08.2017.
 */

public class TimeAdapter extends RecyclerView.Adapter<TimeAdapter.ViewHolder> {
    private OnNotifyClick clickListener;
    private Context context;
    private List<BookingTimes> model;
    ArrayList<String> res = new ArrayList<>();

    public TimeAdapter(Context ctx, OnNotifyClick listener, List<BookingModel> m) {
        context = ctx;
        model = convertModel(m);
        clickListener = listener;
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item_time, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mTime.setText(model.get(position).getTime());
        if (!model.get(position).isBooked){ // not booked layout
            holder.mState.setText(R.string.booking_not_booked);
            holder.mState.setTextColor(context.getResources().getColor(R.color.not_booked));

            // if waiting for approve
            if (model.get(position).isWaiting){
                holder.mState.setText(R.string.booking_state_waiting);
                holder.mState.setTextColor(context.getResources().getColor(R.color.waiting));
            }

        } else { // booked layout
            holder.mOrderCheckbox.setVisibility(View.GONE);
            holder.mNotify.setVisibility(View.VISIBLE);
            holder.mNotify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.OnNotifyClick(model.get(position).getTime());
                }
            });
            holder.mState.setText(R.string.booking_state_booked);
            holder.mState.setTextColor(context.getResources().getColor(R.color.booked));

        }
        // when checking checkbox adding the checkbox to array with checked checkboxes
        // (or removing)
        holder.mOrderCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    res.add(holder.mTime.getText().toString());
                } else {
                    res.remove(holder.mTime.getText().toString());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return model.size();
    }

    public void setData(List<BookingModel> data) {
        model = convertModel(data);
        notifyDataSetChanged();
    }

    public ArrayList<String> getChecked() {
        return res;
    }

    // after successful booking refreshing list
    public void refresh() {
        for (BookingTimes b:model){
            for (String s:res){
                if (b.getTime().equals(s)){
                    b.setWaiting(true);
                    res.remove(s);
                    break;
                }
            }
        }
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView mTime;
        TextView mState;
        CheckBox mOrderCheckbox;
        Button mNotify;
        public ViewHolder(View itemView) {
            super(itemView);
            mTime = itemView.findViewById(R.id.tv_time);
            mState = itemView.findViewById(R.id.tv_state);
            mOrderCheckbox = itemView.findViewById(R.id.ch_order);
            mNotify = itemView.findViewById(R.id.btn_notify_me);
        }
    }

    // casting model
    private List<BookingTimes> convertModel(List<BookingModel> m) {
        ArrayList<BookingTimes> list = new ArrayList<>();
        for (String s: Constants.TIMES_TO_DISPLAY){
            list.add(new BookingTimes(s, false));
        }
        // if data is null - make empty list
        if (m == null) return list;
        // converting data to more useful format
        for (BookingModel b: m){
            for (BookingTimes l: list){
                if (Utils.parseTime(b.getTime()).equals(l.getTime())){
                    if (b.isWaiting()) l.setWaiting(true); else l.setBooked(true);
                    break;
                }
            }
        }

        return list;
    }

    private class BookingTimes{
        private String time;

        private boolean isBooked;
        private boolean isWaiting;

        public BookingTimes(String s, boolean b) {
            time = s;
            isBooked = b;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public boolean isBooked() {
            return isBooked;
        }

        public void setBooked(boolean booked) {
            isBooked = booked;
        }

        public boolean isWaiting() {
            return isWaiting;
        }

        public void setWaiting(boolean waiting) {
            isWaiting = waiting;
        }
    }
}
