package com.antipov.brutaltech.ui.fragment.my_booking;

import android.content.Context;


import com.antipov.brutaltech.API.BookingAPI;
import com.antipov.brutaltech.R;
import com.antipov.brutaltech.model.response.BookingResponseModel;
import com.antipov.brutaltech.utils.Constants;
import com.antipov.brutaltech.utils.RetrofitUtils;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Antipov on 12.09.2017.
 */

public class MyBookingsFragmentInteractorImpl implements MyBookingsFragmentInteractor {
    @Override
    public void getBookings(final Context context, String token, String key, int page,
                            final OnGetBookings onGetBookings, final OnNoInternet onNoInternet) {
        Observable<BookingResponseModel> call = RetrofitUtils.getApiBookingApiClient().
                create(BookingAPI.class).getByKey(token, key, page);
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(Constants.RETRY_COUNT)
                .subscribe(new Subscriber<BookingResponseModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e.toString().contains(context.getString(R.string.err_no_internet))) {
                            onNoInternet.onNoInternet();
                        } else {
                            onGetBookings.onGetBookingsFailure(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(BookingResponseModel model) {
                        if (model != null && model.isResponseSuccessful()){
                            onGetBookings.onGetBookingsSuccess(model);
                        } else {
                            onGetBookings.onGetBookingsFailure(model.getError());
                        }
                    }
                });
    }
}
