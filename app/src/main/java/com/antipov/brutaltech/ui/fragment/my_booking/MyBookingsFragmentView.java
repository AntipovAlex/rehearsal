package com.antipov.brutaltech.ui.fragment.my_booking;

import com.antipov.brutaltech.model.response.BookingResponseModel;

/**
 * Created by Antipov on 12.09.2017.
 */

public interface MyBookingsFragmentView {
    interface OnGetBookings{
        void onGetBookingsSuccess(BookingResponseModel model);

        void onGetBookingsFailure(String error);
    }
}
