package com.antipov.brutaltech.ui.activity.main;

import android.content.Context;

import com.antipov.brutaltech.model.UserModel;
import com.antipov.brutaltech.model.response.ActionResponseModel;

/**
 * Created by Antipov on 04.09.17.
 */

public class MainPresenterImpl implements MainPresenter, MainInteractor.OnNoInternet, MainInteractor.OnSignin, MainInteractor.OnRegistration {

    private final MainInteractorImpl mInteractor;
    private MainActivity mView;

    public MainPresenterImpl(MainActivity mainActivity) {
        mView = mainActivity;
        mInteractor = new MainInteractorImpl();
    }

    @Override
    public void signin(Context context, String login, String password, String deviceToken) {
        mInteractor.signin(context, login, password, deviceToken, this, this);
    }

    @Override
    public void register(Context context, String username, String email, String password, String firstname, String lastname, String phone) {
        mInteractor.registration(context, username, email, password, firstname, lastname, phone, this, this);
    }

    @Override
    public void onSigninSuccess(UserModel model) {
        if (mView != null){
            mView.onSigninSuccess(model);
        }
    }

    @Override
    public void onSigninFailure(String error) {
        if (mView != null){
            mView.onSigninFailure(error);
        }
    }

    @Override
    public void onRegistrationSuccess(ActionResponseModel model) {
        if (mView != null){
            mView.onRegistrationSuccess(model);
        }
    }

    @Override
    public void onRegistrationFailure(String errors) {
        if (mView != null){
            mView.onRegistrationFailure(errors);
        }
    }

    @Override
    public void onNoInternet() {
        if (mView != null){
            mView.onNoInternet();
        }
    }

    @Override
    public void onDestroy() {
        if (mView != null){
            mView = null;
        }
    }
}
