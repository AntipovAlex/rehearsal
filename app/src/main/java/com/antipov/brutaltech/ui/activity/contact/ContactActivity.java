package com.antipov.brutaltech.ui.activity.contact;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.antipov.brutaltech.R;
import com.antipov.brutaltech.model.UserModel;
import com.antipov.brutaltech.ui.activity.base.BaseActivity;
import com.antipov.brutaltech.utils.DialogUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class ContactActivity extends BaseActivity implements OnMapReadyCallback, View.OnClickListener {
    private GoogleMap mMap;
    private Toolbar toolbar;
    private boolean isInfoPopupShown = true;
    private boolean isOkClicked = false;
    private View.OnClickListener okListener;
    private Snackbar snackbar = null;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initMap();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_contact;
    }

    @Override
    protected void initViews() {
        toolbar =  findViewById(R.id.toolbar);
        fab = findViewById(R.id.floatingActionButton);
    }


    @Override
    protected void initListeners() {
        okListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isOkClicked = true;
            }
        };
        fab.setOnClickListener(this);
    }

    @Override
    protected void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setSubtitle(R.string.nav_contacts);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void initPresenter() {

    }

    private void initMap() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Add a marker in Sydney and move the camera
        LatLng coordinates = new LatLng(48.4592834, 35.0110404);
        final Marker marker = mMap.addMarker(new MarkerOptions()
                .position(coordinates).title(getString(R.string.app_name)).snippet(getString(R.string.map_snippet)));
        marker.showInfoWindow();
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 15.0f));
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                // determining info popup position
                if (isInfoPopupShown){
                    isInfoPopupShown = !isInfoPopupShown;
                    onInfopopupStateChanged(isInfoPopupShown);
                }
            }
        });
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                // determining info popup position
                if (!isInfoPopupShown){
                    isInfoPopupShown = !isInfoPopupShown;
                    onInfopopupStateChanged(isInfoPopupShown);
                }
                return false;
            }
        });
    }

    // showing or hiding snacbar with fab
    private void onInfopopupStateChanged(boolean isVisible){
        if (!isVisible){
            fab.show();
            if (!isOkClicked){
                snackbar = DialogUtils.showSnackbar(this, getString(R.string.contact_help), getString(R.string.ok), okListener, Snackbar.LENGTH_INDEFINITE);
                snackbar.show();
            }
        } else {
            fab.hide();
            if (snackbar != null){
                snackbar.dismiss();
                snackbar = null;
            }
        }
    }

    @Override
    public void ifAuthenticated(UserModel model) {

    }

    @Override
    public void ifNotAuthenticated() {

    }

    @Override
    public void onNoInternet() {

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.floatingActionButton:
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:")); // only email apps should handle this
                intent.putExtra(Intent.EXTRA_EMAIL, new String[] {getString(R.string.studio_email)});
                intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.message_topic));
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
                break;
        }
    }
}
