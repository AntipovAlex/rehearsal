package com.antipov.brutaltech.ui.fragment.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.antipov.brutaltech.R;

/**
 * Created by Antipov on 25.08.2017.
 */

public abstract class BaseFragment extends Fragment {
    protected View mView;
    protected Context context;
    private MaterialDialog materialDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(getLayoutId(), container, false);
            context = getContext();
        }
        getArgsFromBundle();
        initPresenter();
        initViews();
        initListener();
        return mView;
    }

    public void showProgress(){
        materialDialog = new MaterialDialog.Builder(context)
                .title(R.string.progress_dialog_title)
                .content(R.string.please_wait)
                .progress(true, 0)
                .cancelable(false)
                .show();
    }

    public void hideProgress(){
        if (materialDialog != null){
            materialDialog.dismiss();
        }
    }

    public abstract int getLayoutId();

    public abstract void getArgsFromBundle();

    public abstract void initPresenter();

    public abstract void initViews();

    public abstract void initListener();

    public abstract void onNoInternet();
}
