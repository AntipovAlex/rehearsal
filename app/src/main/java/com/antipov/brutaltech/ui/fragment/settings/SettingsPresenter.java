package com.antipov.brutaltech.ui.fragment.settings;

import android.content.Context;

/**
 * Created by Antipov on 17.09.2017.
 */

interface SettingsPresenter {
    void updateUser(Context context, String token, String email, String fname, String sname, String phone);

    void onDestroy();
}
