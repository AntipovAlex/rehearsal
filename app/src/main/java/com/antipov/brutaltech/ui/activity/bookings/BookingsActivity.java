package com.antipov.brutaltech.ui.activity.bookings;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.antipov.brutaltech.R;
import com.antipov.brutaltech.interfaces.OnFragmentShowed;
import com.antipov.brutaltech.model.UserModel;
import com.antipov.brutaltech.ui.activity.base.BaseActivity;
import com.antipov.brutaltech.ui.adapter.BookingsPagerAdapter;

public class BookingsActivity extends BaseActivity {

    private ViewPager mPager;
    private TabLayout mTabs;
    private BookingsPagerAdapter adapter;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_bookings;
    }


    @Override
    protected void initViews() {
        toolbar =  findViewById(R.id.toolbar);
        mPager = findViewById(R.id.vp_bookings);
        mTabs = findViewById(R.id.tab_layout);
        mTabs.addTab(mTabs.newTab().setText(R.string.past));
        mTabs.addTab(mTabs.newTab().setText(R.string.today));
        mTabs.addTab(mTabs.newTab().setText(R.string.future));
        adapter = new BookingsPagerAdapter(getSupportFragmentManager(), context);
        mPager.setOffscreenPageLimit(3);
        mPager.setAdapter(adapter);
        mPager.setCurrentItem(1);
        mTabs.setupWithViewPager(mPager);
    }

    @Override
    protected void initListeners() {
        mPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabs));
        mTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Fragment fragment = (Fragment) adapter.instantiateItem(mPager, tab.getPosition());
                if (fragment instanceof OnFragmentShowed){
                    ((OnFragmentShowed)fragment).onFragmentShowed();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    protected void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setSubtitle(R.string.nav_my_booking);
    }

    @Override
    protected void initPresenter() {

    }

    @Override
    public void ifAuthenticated(UserModel model) {

    }

    @Override
    public void ifNotAuthenticated() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onNoInternet() {

    }
}
