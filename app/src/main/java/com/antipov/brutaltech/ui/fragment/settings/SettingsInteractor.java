package com.antipov.brutaltech.ui.fragment.settings;

import android.content.Context;

import com.antipov.brutaltech.model.response.ActionResponseModel;
import com.antipov.brutaltech.model.response.BookingResponseModel;

/**
 * Created by Antipov on 17.09.2017.
 */

interface SettingsInteractor {
    void updateUser(Context context, String token, String email, String fname, String sname,
                    String phone, OnNoInternet onNoInternet, OnUpdateUser onUpdateUser);

    interface OnUpdateUser{
        void onUpdateUserSuccess(ActionResponseModel model);

        void onUpdateUserFailure(String error);
    }

    interface OnNoInternet{
        void onNoInternet();
    }

}
