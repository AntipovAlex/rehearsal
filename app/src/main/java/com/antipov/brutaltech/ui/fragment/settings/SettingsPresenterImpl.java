package com.antipov.brutaltech.ui.fragment.settings;

import android.content.Context;

import com.antipov.brutaltech.model.response.ActionResponseModel;

/**
 * Created by Antipov on 17.09.2017.
 */

public class SettingsPresenterImpl implements SettingsPresenter, SettingsInteractor.OnUpdateUser, SettingsInteractor.OnNoInternet {
    private SettingsFragment mView;
    private final SettingsInteractorImpl mInteractor;

    public SettingsPresenterImpl(SettingsFragment settingsFragment) {
        mView = settingsFragment;
        mInteractor = new SettingsInteractorImpl();
    }

    @Override
    public void updateUser(Context context, String token, String email, String fname, String sname, String phone) {
        mInteractor.updateUser(context, token, email, fname, sname, phone, this, this);
    }

    @Override
    public void onUpdateUserSuccess(ActionResponseModel model) {
        if (mView != null){
            mView.onUpdateUserSuccess(model);
        }
    }

    @Override
    public void onUpdateUserFailure(String error) {
        if (mView != null){
            mView.onUpdateUserFailure(error);
        }
    }

    @Override
    public void onNoInternet() {
        if (mView != null){
            mView.onNoInternet();
        }
    }

    @Override
    public void onDestroy() {
        if (mView != null){
            mView = null;
        }
    }
}
