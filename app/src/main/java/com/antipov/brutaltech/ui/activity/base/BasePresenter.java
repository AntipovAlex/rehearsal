package com.antipov.brutaltech.ui.activity.base;

import android.content.Context;

/**
 * Created by Antipov on 21.08.2017.
 */

interface BasePresenter {
    void checkAuth(Context ctx, String token);

    void onDestroy();
}
