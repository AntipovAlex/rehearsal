package com.antipov.brutaltech.ui.fragment.calendar;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.CalendarView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.antipov.brutaltech.R;
import com.antipov.brutaltech.interfaces.OnBookedSuccessfully;
import com.antipov.brutaltech.model.UserModel;
import com.antipov.brutaltech.ui.activity.main.MainActivity;
import com.antipov.brutaltech.ui.fragment.base.BaseFragment;
import com.antipov.brutaltech.ui.fragment.time.TimeFragment;
import com.antipov.brutaltech.utils.Constants;
import com.antipov.brutaltech.utils.DialogUtils;
import com.antipov.brutaltech.utils.FragmentUtils;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

/**
 * Created by Antipov on 25.08.2017.
 */

public class CalendarFragment extends BaseFragment {
    MaterialCalendarView calendarView;

    public static CalendarFragment newInstance(UserModel model){
        Bundle args = new Bundle();
        CalendarFragment fragment = new CalendarFragment();
        args.putSerializable(Constants.Intent.MODEL, model);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public int getLayoutId() {
        return R.layout.fragment_calendar;
    }

    @Override
    public void getArgsFromBundle() {

    }

    @Override
    public void initPresenter() {
    }

    @Override
    public void initViews() {
        calendarView = mView.findViewById(R.id.calendarView);
        calendarView.state().edit()
                .setMinimumDate(CalendarDay.today())
                .commit();
        ((MainActivity)getActivity()).getSupportActionBar()
                .setSubtitle(context.getString(R.string.step_one_subtitle));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.help_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.toolbar_help:
                DialogUtils.createInformationDialog(context,getString(R.string.help_dialog_title),
                        getString(R.string.help_calendar_content), true, new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                }).show();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void initListener() {
        calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                showTimeActivity(date.getYear(), date.getMonth(), date.getDay());
            }
        });
    }

    @Override
    public void onNoInternet() {

    }

    private void showTimeActivity(int i, int i1, int i2) {
        FragmentUtils.addBackStack(getFragmentManager(), TimeFragment.newInstance(i,i1+1,i2));
    }


}
