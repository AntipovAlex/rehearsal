package com.antipov.brutaltech.ui.activity.base;

import android.content.Context;

import com.antipov.brutaltech.model.UserModel;

/**
 * Created by Antipov on 21.08.2017.
 */

public class BasePresenterImpl implements BasePresenter, BaseInteractor.OnCheckAuth, BaseInteractor.OnNoInternet {
    private final BaseInteractorImp mInteractor;
    private BaseView mView;

    public BasePresenterImpl(BaseView view){
        mView = view;
        mInteractor = new BaseInteractorImp();
    }

    @Override
    public void checkAuth(Context ctx, String token) {
        mInteractor.checkAuth(ctx, token, this, this);
    }

    @Override
    public void onCheckAuthSuccess(UserModel user) {
        if (mView != null){
            mView.ifAuthenticated(user);
        }
    }

    @Override
    public void onCheckAuthFailure(String error) {
        if (mView != null){
            mView.ifNotAuthenticated();
        }
    }

    @Override
    public void onNoInternet() {
        if (mView != null){
            mView.onNoInternet();
        }
    }

    @Override
    public void onDestroy() {
        if (mView != null){
            mView = null;
        }
    }
}
