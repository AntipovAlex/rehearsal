package com.antipov.brutaltech.ui.activity.main;

import com.antipov.brutaltech.model.UserModel;
import com.antipov.brutaltech.model.response.ActionResponseModel;

/**
 * Created by Antipov on 04.09.17.
 */

public interface MainView {
    interface OnRegistration{
        void onRegistrationSuccess(ActionResponseModel model);

        void onRegistrationFailure(String errors);
    }

    interface OnSignin{
        void onSigninSuccess(UserModel model);

        void onSigninFailure(String error);
    }
}
