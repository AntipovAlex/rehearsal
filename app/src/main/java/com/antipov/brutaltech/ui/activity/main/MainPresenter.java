package com.antipov.brutaltech.ui.activity.main;

import android.content.Context;

/**
 * Created by Antipov on 04.09.17.
 */

interface MainPresenter {

    void register(Context context, String username, String email, String password, String firstname, String lastname, String phone);

    void onDestroy();

    void signin(Context context, String login, String password, String deviceToken);
}
