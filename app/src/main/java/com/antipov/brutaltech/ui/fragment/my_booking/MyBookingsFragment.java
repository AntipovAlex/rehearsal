package com.antipov.brutaltech.ui.fragment.my_booking;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.antipov.brutaltech.R;
import com.antipov.brutaltech.interfaces.OnFragmentShowed;
import com.antipov.brutaltech.model.response.BookingResponseModel;
import com.antipov.brutaltech.ui.adapter.BookingsAdapter;
import com.antipov.brutaltech.ui.fragment.base.BaseFragment;
import com.antipov.brutaltech.utils.Constants;
import static com.antipov.brutaltech.App.sessionManager;

/**
 * Created by Antipov on 10.09.2017.
 */

public class MyBookingsFragment extends BaseFragment implements OnFragmentShowed, MyBookingsFragmentView.OnGetBookings {

    private String key;
    private MyBookingsFragmentPresenterImpl mPresenter;
    private RecyclerView mRecycler;
    private BookingsAdapter adapter;
    private RelativeLayout mNoInternetStub;
    private RelativeLayout mErrorStub;
    private Button mRefreshErrButton;
    private Button mRefreshInetButton;
    private ProgressBar mPb;
    private int page = 1;
    private RelativeLayout mEmptyState;
    private Button mGetNewBookings;
    private boolean mLoadMore = true;
    private LinearLayoutManager mLayoutManager;


    public static MyBookingsFragment newInstance(String key){
        Bundle args = new Bundle();
        args.putString("key", key);
        MyBookingsFragment fragment = new MyBookingsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (key.equals(Constants.TODAY)) onFragmentShowed();
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_my_bookings;
    }

    @Override
    public void getArgsFromBundle() {
        key = getArguments().getString("key");
    }

    @Override
    public void onFragmentShowed() {
        loadBookings();
    }

    @Override
    public void initPresenter() {
        mPresenter = new MyBookingsFragmentPresenterImpl(this);
    }

    @Override
    public void initViews() {
        mErrorStub = mView.findViewById(R.id.error_layout);
        mNoInternetStub = mView.findViewById(R.id.no_internet_layout);
        mEmptyState = mView.findViewById(R.id.empty_state_layout);
        mRefreshInetButton = mNoInternetStub.findViewById(R.id.btn_refresh);
        mRefreshErrButton = mErrorStub.findViewById(R.id.btn_refresh);
        mGetNewBookings = mEmptyState.findViewById(R.id.btn_get);
        mPb = mView.findViewById(R.id.pb);
        mRecycler = mView.findViewById(R.id.mybookings_recycler);
        mLayoutManager = new LinearLayoutManager(context);
        mRecycler.setLayoutManager(mLayoutManager);
        adapter = new BookingsAdapter(context);
        mRecycler.setAdapter(adapter);
    }

    @Override
    public void initListener() {
        View.OnClickListener refresh = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mErrorStub.setVisibility(View.GONE);
                mNoInternetStub.setVisibility(View.GONE);
                mPb.setVisibility(View.VISIBLE);
                // TODO: test
                loadBookings();
            }
        };
        mRefreshInetButton.setOnClickListener(refresh);
        mRefreshErrButton.setOnClickListener(refresh);
        mGetNewBookings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });
        mRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy < 0 || !mLoadMore) return;
                final int visibleItemCount = recyclerView.getChildCount();
                final int totalItemCount = mLayoutManager.getItemCount();
                final int firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

                if ((totalItemCount - visibleItemCount) <= (firstVisibleItem + Constants.VISIBLE_ITEMS)) {
                    mLoadMore = false;
                    loadBookings();
                }
            }
        });
    }

    private void loadBookings(){
        if (mLoadMore){
            mPb.setVisibility(View.VISIBLE);
            mPresenter.getBookings(context, sessionManager.getToken(), key, page);
        }
    }

    @Override
    public void onGetBookingsSuccess(BookingResponseModel model) {
        if (page == 1 & model.getData().size() == 0){
            mEmptyState.setVisibility(View.VISIBLE);
            return;
        }
        page++;
        mLoadMore = !(model.getData().size() == 0);
        adapter.add(model);
        mPb.setVisibility(View.GONE);
    }

    @Override
    public void onGetBookingsFailure(String error) {
        mPb.setVisibility(View.GONE);

    }


    @Override
    public void onNoInternet() {
        mPb.setVisibility(View.GONE);
        mNoInternetStub.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroy() {
        mPresenter.onDestroy();
        super.onDestroy();
    }
}
