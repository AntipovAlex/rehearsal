package com.antipov.brutaltech.ui.fragment.settings;

import android.content.Context;

import com.antipov.brutaltech.API.BookingAPI;
import com.antipov.brutaltech.R;
import com.antipov.brutaltech.model.response.ActionResponseModel;
import com.antipov.brutaltech.model.response.BookingResponseModel;
import com.antipov.brutaltech.utils.Constants;
import com.antipov.brutaltech.utils.RetrofitUtils;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Antipov on 17.09.2017.
 */

public class SettingsInteractorImpl implements SettingsInteractor {
    @Override
    public void updateUser(final Context context, String token, String email, String fname, String sname, String phone, final OnNoInternet onNoInternet, final OnUpdateUser onUpdateUser) {
        Observable<ActionResponseModel> call = RetrofitUtils.getApiBookingApiClient().
                create(BookingAPI.class).updateUser(token, email, fname, sname, phone);
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(Constants.RETRY_COUNT)
                .subscribe(new Subscriber<ActionResponseModel>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e.toString().contains(context.getString(R.string.err_no_internet))) {
                            onNoInternet.onNoInternet();
                        } else {
                            onUpdateUser.onUpdateUserFailure(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(ActionResponseModel model) {
                        if (model != null && model.isResponseSuccessful()){
                            onUpdateUser.onUpdateUserSuccess(model);
                        } else {
                            onUpdateUser.onUpdateUserFailure(model.getError());
                        }
                    }
                });
    }
}
