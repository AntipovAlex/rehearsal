package com.antipov.brutaltech.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.antipov.brutaltech.ui.fragment.about.AboutFragment;

/**
 * Created by Antipov on 19.09.17.
 */

public class AboutPagerAdapter extends FragmentStatePagerAdapter {
    private static final int NUM_PAGES = 6;

    public AboutPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return AboutFragment.newInstance(position, NUM_PAGES);
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }
}
