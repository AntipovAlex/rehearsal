package com.antipov.brutaltech.ui.fragment.time;

import android.content.Context;

import com.antipov.brutaltech.API.BookingAPI;
import com.antipov.brutaltech.R;
import com.antipov.brutaltech.model.response.ActionResponseModel;
import com.antipov.brutaltech.model.response.BookingResponseModel;
import com.antipov.brutaltech.utils.Constants;
import com.antipov.brutaltech.utils.RetrofitUtils;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by Antipov on 27.08.2017.
 */

public class TimeFragmentInteractorImpl implements TimeFragmentInteractor {

    @Override
    public void getTime(final Context context, int year, int month, int day, final OnGetTime onGetTimeListener, final OnNoInternet onNoInternetListener) {
        Observable<BookingResponseModel> call = RetrofitUtils.getApiBookingApiClient().
                create(BookingAPI.class).getForDay(String.valueOf(year), String.valueOf(month), String.valueOf(day));
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(Constants.RETRY_COUNT)
                .subscribe(new Subscriber<BookingResponseModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e.toString().contains(context.getString(R.string.err_no_internet))) {
                            onNoInternetListener.onNoInternet();
                        } else {
                            onGetTimeListener.onGetTimeFailure(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(BookingResponseModel model) {
                        if (model != null && model.isResponseSuccessful()){
                            onGetTimeListener.onGetTimeSuccess(model);
                        } else {
                            onGetTimeListener.onGetTimeFailure("Get time error");
                        }
                    }
                });
    }

    @Override
    public void book(final Context context, String token, int year, int month, int day, String times, String name, String tel, String comment, int type, final OnNoInternet onNoInternet, final OnBookTime onBookTime) {
        Observable<ActionResponseModel> call = RetrofitUtils.getApiBookingApiClient().
                create(BookingAPI.class).sendBooking(token, String.valueOf(year),
                String.valueOf(month), String.valueOf(day), times, name, tel, comment, String.valueOf(type));
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(Constants.RETRY_COUNT)
                .subscribe(new Subscriber<ActionResponseModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e.toString().contains(context.getString(R.string.err_no_internet))) {
                            onBookTime.onBookTimeFailure(context.getString(R.string.no_internet_connection));
                        } else {
                            onBookTime.onBookTimeFailure(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(ActionResponseModel model) {
                        if (model != null && model.isResponseSuccessful()){
                            onBookTime.onBookTimeSuccess(model);
                        } else {
                            onBookTime.onBookTimeFailure(model.getError());
                        }
                    }
                });
    }

    @Override
    public void sendNotifyRequest(final Context context, String name, int year, int month, int day,
                                  String time, String phone, final OnSendNotifyRequest onSendNotifyRequest, final OnNoInternet onNoInternet) {
        Observable<ActionResponseModel> call = RetrofitUtils.getApiBookingApiClient().
                create(BookingAPI.class).sendNotifyRequest(name, String.valueOf(year),
                String.valueOf(month), String.valueOf(day), time, phone);
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(Constants.RETRY_COUNT)
                .subscribe(new Subscriber<ActionResponseModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e.toString().contains(context.getString(R.string.err_no_internet))) {
                            onNoInternet.onNoInternet();
                        } else {
                            onSendNotifyRequest.onSendNotifyRequestFailure(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(ActionResponseModel model) {
                        if (model != null && model.isResponseSuccessful()){
                            onSendNotifyRequest.onSendNotifyRequestSuccess(model);
                        } else {
                            onSendNotifyRequest.onSendNotifyRequestFailure(model.getError());
                        }
                    }
                });
    }
}
