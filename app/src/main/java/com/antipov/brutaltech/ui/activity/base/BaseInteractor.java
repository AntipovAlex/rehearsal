package com.antipov.brutaltech.ui.activity.base;

import android.content.Context;

import com.antipov.brutaltech.model.UserModel;

/**
 * Created by Antipov on 21.08.2017.
 */

interface BaseInteractor {
    void checkAuth(Context context, String token, OnCheckAuth onCheckAuthListener, OnNoInternet onNoInternetListener);

    interface OnCheckAuth{
        void onCheckAuthSuccess(UserModel user);

        void onCheckAuthFailure(String error);
    }

    interface OnNoInternet{
        void onNoInternet();
    }
}
