package com.antipov.brutaltech.ui.fragment.settings;

import android.app.Activity;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.antipov.brutaltech.BuildConfig;
import com.antipov.brutaltech.R;
import com.antipov.brutaltech.model.response.ActionResponseModel;
import com.antipov.brutaltech.ui.activity.base.BaseActivity;
import com.antipov.brutaltech.utils.Constants;
import com.antipov.brutaltech.utils.DialogUtils;
import com.antipov.brutaltech.utils.helpers.LocaleHelper;
import com.antipov.brutaltech.utils.managers.UserInfoManager;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.antipov.brutaltech.App.sessionManager;

/**
 * Created by Antipov on 06.09.17.
 */

public class SettingsFragment extends PreferenceFragment implements SettingsView, SettingsView.OnUpdateUser {
    private int index = 0;
    private UserInfoManager userManager;
    private String[] languages;
    private String current;
    private ListPreference languagePreference;
    private EditTextPreference emailPreference;
    private EditTextPreference fnamePreference;
    private EditTextPreference snamePreference;
    private EditTextPreference phonePreference;
    private Preference versionPreference;

    private static final String VERSION = "version";
    private static final String LANGUAGE = "language";
    private static final String EMAIL = "email";
    private static final String FNAME = "firstname";
    private static final String SNAME = "secondname";
    private static final String PHONE = "phone";
    private Menu mMenu;
    private SettingsPresenterImpl mPresenter;


    public static SettingsFragment newInstance(){
        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
        // set up user manager
        userManager = new UserInfoManager(getActivity().getApplicationContext());
        // determining current language
        current = (getResources().getConfiguration().locale).getLanguage();
        // determining current language array-index
        languages = getActivity().getResources().getStringArray(R.array.languagesValues);

        setHasOptionsMenu(true);

        initViews();
        initListeners();
        initPresenter();
    }

    @Override
    public void initViews() {
        // binding
        versionPreference = getPreferenceManager().findPreference(VERSION);
        languagePreference = (ListPreference) getPreferenceManager().findPreference(LANGUAGE);
        emailPreference = (EditTextPreference) getPreferenceManager().findPreference(EMAIL);
        fnamePreference = (EditTextPreference) getPreferenceManager().findPreference(FNAME);
        snamePreference = (EditTextPreference) getPreferenceManager().findPreference(SNAME);
        phonePreference = (EditTextPreference) getPreferenceManager().findPreference(PHONE);

        // set defaults
        emailPreference.setSummary(userManager.getEmail());
        emailPreference.setText(userManager.getEmail());
        fnamePreference.setSummary(userManager.getFname());
        fnamePreference.setText(userManager.getFname());
        snamePreference.setSummary(userManager.getSname());
        snamePreference.setText(userManager.getSname());
        phonePreference.setSummary(userManager.getPhone());
        phonePreference.setText(userManager.getPhone());
        versionPreference.setSummary(BuildConfig.VERSION_NAME);

        for (int i = 0; i <= languages.length-1; i++){
            if (languages[i].equals(current)) {
                index = i;
                break;
            }
        }
        languagePreference.setValueIndex(index);
    }

    @Override
    public void initListeners() {
        languagePreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                LocaleHelper.setLocale(getActivity(), o.toString());
                Resources res = getResources();
                // Change locale settings in the app.
                DisplayMetrics dm = res.getDisplayMetrics();
                Configuration conf = res.getConfiguration();
                conf.locale = new Locale(o.toString());
                res.updateConfiguration(conf, dm);
                getActivity().recreate();
                return false;
            }
        });
        Preference.OnPreferenceChangeListener changeListener = new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                switch (preference.getKey()){
                    case EMAIL:
                        if(!validateEmail(o.toString())){
                            DialogUtils.showSnackbar((AppCompatActivity)getActivity(), getString(R.string.bad_email));
                            return false;
                        }
                        break;
                    case FNAME:
                        if(!validateFname(o.toString())){
                            DialogUtils.showSnackbar((AppCompatActivity)getActivity(), getString(R.string.bad_name));
                            return false;
                        }
                        break;
                    case SNAME:
                        if(!validateSname(o.toString())){
                            DialogUtils.showSnackbar((AppCompatActivity)getActivity(), getString(R.string.bad_name));
                            return false;
                        }
                        break;
                    case PHONE:
                        if(!validatePhone(o.toString())){
                            DialogUtils.showSnackbar((AppCompatActivity)getActivity(), getString(R.string.bad_phone));
                            return false;
                        }
                        break;
                }
                preference.setSummary(o.toString());
                mMenu.setGroupVisible(R.id.save, true);
                return false;
            }
        };
        emailPreference.setOnPreferenceChangeListener(changeListener);
        fnamePreference.setOnPreferenceChangeListener(changeListener);
        snamePreference.setOnPreferenceChangeListener(changeListener);
        phonePreference.setOnPreferenceChangeListener(changeListener);
    }

    @Override
    public void initPresenter() {
        mPresenter = new SettingsPresenterImpl(this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.save_menu, menu);
        menu.setGroupVisible(R.id.save, false);
        mMenu = menu;
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.toolbar_save:
                ((BaseActivity)getActivity()).showProgress();
                mPresenter.updateUser(getActivity().getApplicationContext(), sessionManager.getToken(),
                        emailPreference.getSummary().toString(), fnamePreference.getSummary().toString(),
                        snamePreference.getSummary().toString(), phonePreference.getSummary().toString());
                break;
        }
        return true;
    }

    private boolean validatePhone(String phone) {
        Matcher m = Pattern.compile(Constants.REG_EXP_PHONE).matcher(phone);
        return m.matches();
    }

    private boolean validateSname(String sname) {
        Matcher m = Pattern.compile(Constants.REG_EXP_NAME).matcher(sname);
        return m.matches();
    }

    private boolean validateFname(String fname) {
        Matcher m = Pattern.compile(Constants.REG_EXP_NAME).matcher(fname);
        return m.matches();
    }

    private boolean validateEmail(String email) {
        Matcher m = Pattern.compile(Constants.REG_EXP_EMAIL).matcher(email);
        return m.matches();
    }

    @Override
    public void onUpdateUserSuccess(ActionResponseModel model) {
        ((BaseActivity)getActivity()).hideProgress();
        userManager.saveUser(
                emailPreference.getSummary().toString(),
                fnamePreference.getSummary().toString(),
                snamePreference.getSummary().toString(),
                phonePreference.getSummary().toString()
        );
        getActivity().setResult(Activity.RESULT_OK);
        getActivity().finish();
    }

    @Override
    public void onUpdateUserFailure(String error) {
        ((BaseActivity)getActivity()).hideProgress();
        DialogUtils.showSnackbar((AppCompatActivity)getActivity(), error);
    }

    @Override
    public void onNoInternet() {
        ((BaseActivity)getActivity()).hideProgress();
        DialogUtils.showSnackbar((AppCompatActivity)getActivity(), getActivity().getString(R.string.no_internet_connection));
    }

    @Override
    public void onDestroy() {
        mPresenter.onDestroy();
        super.onDestroy();
    }
}
