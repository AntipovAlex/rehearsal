package com.antipov.brutaltech.ui.fragment.time;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.antipov.brutaltech.R;
import com.antipov.brutaltech.interfaces.OnBookedSuccessfully;
import com.antipov.brutaltech.interfaces.OnNotifyClick;
import com.antipov.brutaltech.model.response.ActionResponseModel;
import com.antipov.brutaltech.model.response.BookingResponseModel;
import com.antipov.brutaltech.ui.activity.main.MainActivity;
import com.antipov.brutaltech.ui.adapter.TimeAdapter;
import com.antipov.brutaltech.ui.fragment.base.BaseFragment;
import com.antipov.brutaltech.utils.Constants;
import com.antipov.brutaltech.utils.DialogUtils;
import com.antipov.brutaltech.utils.managers.UserInfoManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.antipov.brutaltech.App.sessionManager;
import static com.antipov.brutaltech.utils.Constants.MAX_TEXT_LEN;

/**
 * Created by Antipov on 27.08.2017.
 */

public class TimeFragment extends BaseFragment implements TimeFragmentView, TimeFragmentView.OnGetTime,
        TimeFragmentView.OnBookTime, OnNotifyClick, TimeFragmentView.OnSendNotifyRequest{
    private TimeFragmentPresenterImpl mPresenter;
    private int year;
    private int month;
    private int day;
    private BookingResponseModel mModel;
    private RecyclerView mRecycler;
    private LinearLayoutManager mLayoutManager;
    private TimeAdapter mAdapter;
    private RelativeLayout mNoInternetStub;
    private RelativeLayout mErrorStub;
    private TextView mErrorText;
    private Button mRefreshInetButton;
    private ProgressBar mPb;
    private Button mRefreshErrButton;
    private TextView mDate;
    private FloatingActionButton fab;
    private MaterialDialog md;
    private MaterialDialog dialogNotify;
    private MaterialDialog.SingleButtonCallback notifyListener;

    public static TimeFragment newInstance(int year, int month, int day){
        Bundle args = new Bundle();
        TimeFragment fragment = new TimeFragment();
        args.putInt(Constants.Intent.YEAR, year);
        args.putInt(Constants.Intent.MONTH, month);
        args.putInt(Constants.Intent.DAY, day);
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPb.setVisibility(View.VISIBLE);
        setHasOptionsMenu(true);
        mPresenter.getTime(context, year, month, day);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.help_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.toolbar_help:
                // making help popup
                DialogUtils.createInformationDialog(context,getString(R.string.help_dialog_title),
                        getString(R.string.help_time_content), true, new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        }).show();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_time;
    }

    @Override
    public void getArgsFromBundle() {
        Bundle b = getArguments();
        year = b.getInt(Constants.Intent.YEAR);
        month = b.getInt(Constants.Intent.MONTH);
        day = b.getInt(Constants.Intent.DAY);
    }

    @Override
    public void initPresenter() {
        mPresenter = new TimeFragmentPresenterImpl(this);
    }

    @Override
    public void initViews() {
        mRecycler = mView.findViewById(R.id.rv_time_list);
        mRecycler.setHasFixedSize(true);
        mDate = mView.findViewById(R.id.tv_date);
        fab = mView.findViewById(R.id.fab);
        mLayoutManager = new LinearLayoutManager(context);
        mRecycler.setLayoutManager(mLayoutManager);
        mNoInternetStub = mView.findViewById(R.id.no_internet_layout);
        mErrorStub = mView.findViewById(R.id.error_layout);
        mErrorText = mErrorStub.findViewById(R.id.tv_error_msg);
        mRefreshInetButton = mNoInternetStub.findViewById(R.id.btn_refresh);
        mRefreshErrButton = mErrorStub.findViewById(R.id.btn_refresh);
        mPb = mView.findViewById(R.id.pb);
        ((MainActivity)getActivity()).getSupportActionBar()
                .setSubtitle(context.getString(R.string.step_two_subtitle));
        mDate.setText(day+"."+month+"."+year);
    }

    @Override
    public void initListener() {
        // refresh button
        View.OnClickListener refresh = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mErrorStub.setVisibility(View.GONE);
                mNoInternetStub.setVisibility(View.GONE);
                mPb.setVisibility(View.VISIBLE);
                mPresenter.getTime(context, year, month, day);
            }
        };
        mRefreshInetButton.setOnClickListener(refresh);
        mRefreshErrButton.setOnClickListener(refresh);
        // hide fab when scrolling
        mRecycler.addOnScrollListener(new RecyclerView.OnScrollListener(){
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy){
                if (dy > 0)
                    fab.hide();
                else if (dy < 0)
                    fab.show();
            }
        });
        // booking time
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ArrayList<String> checked = mAdapter.getChecked();
                if (checked.size() == 0){ // if checked times less than 1 -- show snackbar
                    DialogUtils.showSnackbar((AppCompatActivity)getActivity(), getString(R.string.minimum_one_hour));
                } else { // else making string for request
                    final StringBuilder sb = new StringBuilder();
                    for (Iterator<String> i = checked.iterator(); i.hasNext();){
                        sb.append(i.next());
                        if (i.hasNext()){
                            sb.append(", ");
                        }
                    }
                    // creating dialog for booking
                    md = DialogUtils.createCustomDialog(context, getString(R.string.booking_dialog_title),
                            R.layout.dialog_book, false,
                            new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    TextView name = (TextView)dialog.findViewById(R.id.tv_name);
                                    TextView phone = (TextView) dialog.findViewById(R.id.tv_phone);
                                    TextView comment = (TextView)dialog.findViewById(R.id.tv_comment);



                                    if (validateFields(name, phone, comment)){
                                        showProgress();
                                        book(sb.toString().replaceAll("\\.00", ""),
                                                name.getText().toString().trim(),
                                                phone.getText().toString().trim(),
                                                comment.getText().toString().trim(),
                                                ((Spinner)dialog.findViewById(R.id.spinner)).getSelectedItemPosition()
                                        );
                                    }
                                }
                            },
                            getString(R.string.cancel),
                            getString(R.string.ok)).show();

                    // autofill view if user is authenticated
                    if (sessionManager.isLoggedIn()){
                        UserInfoManager userInfoManager = new UserInfoManager(context);
                        ((TextView)md.findViewById(R.id.tv_name)).setText(userInfoManager.getFname()+ " "+ userInfoManager.getSname());
                        ((TextView)md.findViewById(R.id.tv_phone)).setText(userInfoManager.getPhone());
                    }
                    ((TextView)md.findViewById(R.id.tv_time)).setText(sb);
                }
            }
        });
    }

    private void book(String time, String name, String phone, String comment, int type) {
        StringBuilder sb = new StringBuilder();
        sb.append("");
        // if user logged in - get token from prefs
        if (sessionManager.isLoggedIn()){
            sb.append(sessionManager.getToken());
        }
        mPresenter.book(context, sb.toString(), year, month, day, time.replaceAll(" ", ""), name, phone, comment, type);
    }

    private boolean validateFields(TextView name, TextView phone, TextView comment) {
        Matcher m = Pattern.compile(Constants.REG_EXP_NAME).matcher(name.getText().toString().trim());
        if (!m.matches()) {
            name.setError(getString(R.string.bad_name));
            return false;
        }
        m = Pattern.compile(Constants.REG_EXP_PHONE).matcher(phone.getText().toString().trim());
        if (!m.matches()) {
            phone.setError(getString(R.string.bad_phone));
            return false;
        }
        if (comment != null){
            if (comment.getText().toString().trim().length() >  MAX_TEXT_LEN){
                comment.setError(getString(R.string.text_is_longer, String.valueOf(MAX_TEXT_LEN)));
                return false;
            }
        }

        return true;
    }


    @Override
    public void OnNotifyClick(final String time) {
        if (dialogNotify == null){
            dialogNotify = DialogUtils.createCustomDialog(context, getString(R.string.notify_me),
                    R.layout.dialog_notify, false, new MaterialDialog.SingleButtonCallback(){
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            TextView name = (TextView)dialog.findViewById(R.id.et_name);
                            TextView phone = (TextView) dialog.findViewById(R.id.et_phone);

                            if (validateFields(name, phone, null)){
                                showProgress();
                                mPresenter.sendNotifyRequest(context, name.getText().toString().trim(),
                                        year, month, day, time, phone.getText().toString().trim()
                                );
                            }
                        }
                    }, getString(R.string.cancel), getString(R.string.ok)).show();
        } else {
            dialogNotify.show();
        }
    }

    @Override
    public void onGetTimeSuccess(BookingResponseModel model) {
        mModel = model;
        mAdapter = new TimeAdapter(context, this, mModel.getData());
        mRecycler.setAdapter(mAdapter);
        mPb.setVisibility(View.GONE);
    }

    @Override
    public void onGetTimeFailure(String error) {
        mErrorStub.setVisibility(View.VISIBLE);
        mErrorText.setText(error);
        mPb.setVisibility(View.GONE);
    }

    @Override
    public void onBookTimeSuccess(ActionResponseModel model) {
        hideProgress();
        md.dismiss();
        DialogUtils.showSnackbar((AppCompatActivity) getActivity(), getString(R.string.booked_successfully), getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        }, Snackbar.LENGTH_INDEFINITE).show();
        // DialogUtils.showSnackbar((AppCompatActivity)getActivity(), getString(R.string.booked_successfully));
        // if time booked for today displaying notification
        if (bookedToday()){
            ((OnBookedSuccessfully)getActivity()).onBookedSuccessfully(mAdapter.getChecked().size());
        }
        mAdapter.refresh();
    }

    private boolean bookedToday() {
        // parse date of booking
        SimpleDateFormat formatter = new SimpleDateFormat("d/MM/yyyy");
        String dateBook = day+"/"+month+"/"+year;
        String dateToday = formatter.format(new Date());
        return dateBook.equals(dateToday);
    }

    @Override
    public void onBookTimeFailure(String error) {
        hideProgress();
        DialogUtils.showSnackbar((AppCompatActivity)getActivity(), error);
    }

    @Override
    public void onSendNotifyRequestSuccess(ActionResponseModel model) {
        dialogNotify.dismiss();
        hideProgress();
        DialogUtils.showSnackbar((AppCompatActivity) getActivity(), getString(R.string.booked_successfully), getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        }, Snackbar.LENGTH_INDEFINITE).show();
    }

    @Override
    public void onSendNotifyRequestFailure(String error) {
        hideProgress();
        DialogUtils.showSnackbar((AppCompatActivity)getActivity(), error);
    }

    @Override
    public void onNoInternet() {
        mPb.setVisibility(View.GONE);
        mNoInternetStub.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.onDestroy();
    }
}