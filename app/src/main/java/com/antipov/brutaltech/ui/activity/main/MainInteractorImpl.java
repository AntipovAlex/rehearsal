package com.antipov.brutaltech.ui.activity.main;

import android.content.Context;

import com.antipov.brutaltech.API.BookingAPI;
import com.antipov.brutaltech.R;
import com.antipov.brutaltech.model.response.ActionResponseModel;
import com.antipov.brutaltech.model.response.UserResponseModel;
import com.antipov.brutaltech.utils.Constants;
import com.antipov.brutaltech.utils.RetrofitUtils;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Antipov on 04.09.17.
 */

public class MainInteractorImpl implements MainInteractor {

    @Override
    public void signin(final Context context, String login, String password, String deviceToken, final OnNoInternet onNoInternet, final OnSignin onSignin) {
        Observable<UserResponseModel> call = RetrofitUtils.getApiBookingApiClient().
                create(BookingAPI.class).login(login, password, deviceToken);
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(Constants.RETRY_COUNT)
                .subscribe(new Subscriber<UserResponseModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e.toString().contains(context.getString(R.string.err_no_internet))) {
                            onNoInternet.onNoInternet();
                        } else {
                            onSignin.onSigninFailure(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UserResponseModel model) {
                        if (model != null && model.isResponseSuccessful()){
                            onSignin.onSigninSuccess(model.getData());
                        } else {
                            onSignin.onSigninFailure(model.getError());
                        }
                    }
                });
    }

    @Override
    public void registration(final Context context, String username, String email, String password, String firstname, String lastname, String phone, final OnRegistration onRegistration, final OnNoInternet onNoInternet) {
        Observable<ActionResponseModel> call = RetrofitUtils.getApiBookingApiClient().
                create(BookingAPI.class).register(username, email, password, firstname, lastname,
                phone);
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(Constants.RETRY_COUNT)
                .subscribe(new Subscriber<ActionResponseModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e.toString().contains(context.getString(R.string.err_no_internet))) {
                            onNoInternet.onNoInternet();
                        } else {
                            onRegistration.onRegistrationFailure(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(ActionResponseModel model) {
                        if (model != null && model.isResponseSuccessful()){
                            onRegistration.onRegistrationSuccess(model);
                        } else {
                            onRegistration.onRegistrationFailure(model.getError());
                        }
                    }
                });
    }
}
