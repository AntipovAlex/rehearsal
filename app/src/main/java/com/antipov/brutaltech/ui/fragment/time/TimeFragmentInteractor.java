package com.antipov.brutaltech.ui.fragment.time;

import android.content.Context;

import com.antipov.brutaltech.model.response.ActionResponseModel;
import com.antipov.brutaltech.model.response.BookingResponseModel;

/**
 * Created by Antipov on 27.08.2017.
 */

interface TimeFragmentInteractor {
    void getTime(Context context, int year, int month, int day, OnGetTime onGetTimeListener,
                 OnNoInternet onNoInternetListener);

    void book(Context context, String token, int year, int month, int day, String times, String name,
              String tel, String comment, int type, OnNoInternet onNoInternet, OnBookTime onBookTime);

    void sendNotifyRequest(Context context,  String name, int year, int month, int day, String time, String phone,
                           OnSendNotifyRequest onSendNotifyRequest, OnNoInternet onNoInternet);

    interface OnBookTime{
        void onBookTimeSuccess(ActionResponseModel model);

        void onBookTimeFailure(String error);
    }

    interface OnGetTime{
        void onGetTimeSuccess(BookingResponseModel model);

        void onGetTimeFailure(String error);
    }

    interface OnSendNotifyRequest{
        void onSendNotifyRequestSuccess(ActionResponseModel model);

        void onSendNotifyRequestFailure(String error);
    }

    interface OnNoInternet{
        void onNoInternet();
    }
}
