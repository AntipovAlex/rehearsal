package com.antipov.brutaltech.ui.fragment.time;

import com.antipov.brutaltech.model.response.ActionResponseModel;
import com.antipov.brutaltech.model.response.BookingResponseModel;

/**
 * Created by Antipov on 27.08.2017.
 */

public interface TimeFragmentView {

    interface OnGetTime{
        void onGetTimeSuccess(BookingResponseModel model);

        void onGetTimeFailure(String error);
    }

    interface OnBookTime{
        void onBookTimeSuccess(ActionResponseModel model);

        void onBookTimeFailure(String error);
    }

    interface OnSendNotifyRequest{
        void onSendNotifyRequestSuccess(ActionResponseModel model);

        void onSendNotifyRequestFailure(String error);
    }

}
