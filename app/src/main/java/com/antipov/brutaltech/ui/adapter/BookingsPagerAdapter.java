package com.antipov.brutaltech.ui.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;

import com.antipov.brutaltech.R;
import com.antipov.brutaltech.ui.fragment.my_booking.MyBookingsFragment;
import com.antipov.brutaltech.utils.Constants;

/**
 * Created by Antipov on 10.09.2017.
 */

public class BookingsPagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 3;
    private final Context context;

    public BookingsPagerAdapter(FragmentManager fm, Context ctx) {
        super(fm);
        context = ctx;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment
                return MyBookingsFragment.newInstance(Constants.PAST);
            case 1: // Fragment # 0 - This will show FirstFragment different title
                return MyBookingsFragment.newInstance(Constants.TODAY);
            case 2: // Fragment # 1 - This will show SecondFragment
                return MyBookingsFragment.newInstance(Constants.INFUTURE);
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return context.getString(R.string.past);
            case 1:
                return context.getString(R.string.today);
            case 2:
                return context.getString(R.string.future);
            default:
                return "getPageTitle err";
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }
}
