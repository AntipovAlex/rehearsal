package com.antipov.brutaltech;

import android.app.Application;

import com.antipov.brutaltech.utils.managers.SessionManager;
import com.flurry.android.FlurryAgent;

import static com.antipov.brutaltech.utils.Constants.FLURRY_API_KEY;

/**
 * Created by Antipov on 21.08.2017.
 */

public class App extends Application {
    public static SessionManager sessionManager;

    @Override
    public void onCreate() {
        super.onCreate();
        // flurry analytics
        if (!BuildConfig.DEBUG){ // for prod
            new FlurryAgent.Builder()
                    .withLogEnabled(true)
                    .build(this, FLURRY_API_KEY);
        }

        // session manager for accessing from any class in app
        sessionManager = new SessionManager(getApplicationContext());
    }

}
