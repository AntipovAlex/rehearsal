package com.antipov.brutaltech.utils;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.antipov.brutaltech.R;
import com.antipov.brutaltech.ui.fragment.time.TimeFragment;

/**
 * Created by Antipov on 25.08.2017.
 */

public class FragmentUtils {
    public static void replaceFragment(FragmentManager fm, Fragment fragment){
        clearBackstack(fm);
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.container, fragment, fragment.getClass().getSimpleName());
        transaction.commit();
    }

    public static void clearBackstack(FragmentManager supportFragmentManager){
        supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    public static void addBackStack(FragmentManager supportFragmentManager, Fragment fragment) {
        FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container,  fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
