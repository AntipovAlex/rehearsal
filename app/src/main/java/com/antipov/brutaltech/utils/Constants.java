package com.antipov.brutaltech.utils;

/**
 * Created by antipov on 13.02.17.
 */

public class Constants {
    public static final int RETRY_COUNT = 3;
    public static final int VISIBLE_ITEMS = 15;

    // "http://10.0.3.2:8080/" - localhost
    public static String BASE_URL_SERVER = "http://31.41.221.148:8080/bt/";
    public static String FLURRY_API_KEY = "CXGZPCX7MFX939M2TDSJ";
    public static int COMMENT_LENGTH = 100;
    public static String TIMES_TO_DISPLAY[] = {"08.00", "09.00", "10.00", "11.00", "12.00", "13.00", "14.00",
            "15.00", "16.00", "17.00", "18.00", "19.00", "20.00", };
    public static final String BOOKED = "Забронировано";
    public static final String NOTBOOKED = "Свободно";
    public static final String DATE_KEY = "date";
    public static final String[] BOOKING_TYPE = {"Репетиция", "Урок", "Другое"}; //type array
    /**
     * WTF is that? see there http://emailregex.com/
     */
    public static final String REG_EXP_EMAIL =
            "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-" +
                    "\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-" +
                    "\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" +
                    "\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]" +
                    "?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*" +
                    "[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\" +
                    "[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    public static final String REG_EXP_NAME = "^[А-яA-z ,.'-]+$";
    public static final String REG_EXP_USERNAME = "^[A-z0-9_-]{3,15}$";
    public static final String REG_EXP_PASSWORD = "^[(A-z)(0-9)(@#$%^&+=)]{6,12}";
    public static final String REG_EXP_PHONE = "(3)80([0-9]{9}$)";
    public static final int MAX_TEXT_LEN = 100;
    public static final String PAST = "past";
    public static final String TODAY = "today";
    public static final String INFUTURE = "infuture";
    public static final String RULES_URL = BASE_URL_SERVER+"rules";
    public static final String SITE_URL = "http://brutaltech.com.ua/";
    public static class Intent{
        public static final String MODEL = "model";
        public static final String YEAR = "year";
        public static final String MONTH = "month";
        public static final String DAY = "day";
        public static final String URL = "url";
        public static final String TITLE = "title";
    }
}
