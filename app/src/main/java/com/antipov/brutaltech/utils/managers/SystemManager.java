package com.antipov.brutaltech.utils.managers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Antipov on 19.09.17.
 */

public class SystemManager {
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "RehearsalBasementSystem";

    // login or not login
    private static final String LAUNCHED_BEFORE = "LAUNCHED_BEFORE";

    public SystemManager(Context context){
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public boolean isFisrstTime(){
        return sharedPreferences.getBoolean(LAUNCHED_BEFORE, true);
    }

    public void setBeforeLaunched(){
        editor.putBoolean(LAUNCHED_BEFORE, false);
        editor.commit();
    }
}
