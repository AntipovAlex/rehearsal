package com.antipov.brutaltech.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by antipov on 13.02.17.
 */

public class Utils {

    public static void allowCancelDialog(Boolean isAllowed, AlertDialog dialog){
        dialog.setCancelable(isAllowed);
        Button buttonNegative = dialog.getButton(Dialog.BUTTON_NEGATIVE);
        Button buttonPositive = dialog.getButton(Dialog.BUTTON_POSITIVE);
        buttonNegative.setEnabled(isAllowed);
        buttonPositive.setEnabled(isAllowed);
    }

    public static String parseDate(String time){
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy", Locale.US);
        format.setTimeZone(TimeZone.getTimeZone("GMT+3:00"));
        Date d = new Date(Long.valueOf(time));
        return format.format(d);
    }

    public static String parseTime(String time) {
        SimpleDateFormat format = new SimpleDateFormat("HH.mm");
        format.setTimeZone(TimeZone.getTimeZone("GMT-5:00"));
        Date d = new Date(Long.valueOf(time));
        return format.format(d);
    }

}
