package com.antipov.brutaltech.utils.managers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Antipov on 13.05.17.
 */

public class DeviceTokenManager {
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "RehearsalBasementDeviceToken";

    // login or not login
    private String DEVICE_TOKEN = "DEVICE_TOKEN";

    public DeviceTokenManager(Context context) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public String getDeviceToken() {
        return sharedPreferences.getString(DEVICE_TOKEN, "");
    }

    public void setDeviceToken(String deviceToken) {
        editor.putString(DEVICE_TOKEN, deviceToken);
        editor.commit();
    }
}
