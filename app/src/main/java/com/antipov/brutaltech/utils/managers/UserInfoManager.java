package com.antipov.brutaltech.utils.managers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Antipov on 17.09.2017.
 */

public class UserInfoManager {
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "RehearsalBasementUserInfo";

    // email
    private static final String EMAIL = "EMAIL";

    // first name
    private static final String FNAME = "FNAME";

    // second name
    private static final String SNAME = "SNAME";

    // phone
    private static final String PHONE = "PHONE";



    public UserInfoManager(Context context){
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public void saveUser(String email, String fname, String sname, String phone){
        editor.putString(EMAIL, email);
        editor.putString(FNAME, fname);
        editor.putString(SNAME, sname);
        editor.putString(PHONE, phone);
        editor.commit();
    }

    public String getEmail(){
        return sharedPreferences.getString(EMAIL, "");
    }

    public String getFname(){
        return sharedPreferences.getString(FNAME, "");
    }

    public String getSname(){
        return sharedPreferences.getString(SNAME, "");
    }

    public String getPhone(){
        return sharedPreferences.getString(PHONE, "");
    }

    public void removeUser(){
        editor.clear();
        editor.commit();
    }

}
