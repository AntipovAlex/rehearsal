package com.antipov.brutaltech.utils.managers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Antipov on 3/5/17.
 */

public class SessionManager {
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "RehearsalBasementSession";

    // login or not login
    private static final String IS_LOGIN = "IS_LOGIN";

    // JSESSIONID
    public static final String HEADER = "Authentication";


    public SessionManager(Context context){
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public boolean isLoggedIn(){
        return  sharedPreferences.getBoolean(IS_LOGIN, false);
    }

    public void logInUser(String token){
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(HEADER, token);
        editor.commit();
    }

    public String getToken(){
        return sharedPreferences.getString(HEADER, "");
    }

    public void removeSession(){
        editor.clear();
        editor.commit();

    }
}

