package com.antipov.brutaltech.API;


import com.antipov.brutaltech.model.response.ActionResponseModel;
import com.antipov.brutaltech.model.response.BookingResponseModel;
import com.antipov.brutaltech.model.response.UserResponseModel;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by antipov on 13.02.17.
 */

public interface BookingAPI {
    @GET("booking/get")
    Observable<BookingResponseModel> getForDay(@Query("year") String year, @Query("month") String month,
                                               @Query("day") String day);
    @POST("booking/set")
    Observable<ActionResponseModel> sendBooking(@Header("Authentication") String authentication,
                                                @Query("year") String year, @Query("month") String month,
                                                @Query("day") String day, @Query("times") String times,
                                                @Query("name") String name, @Query("telephone") String telephone,
                                                @Query("comment") String comment,
                                                @Query("type") String type);

    @GET("user/me")
    Observable<UserResponseModel> getUserData(@Header("Authentication") String token);

    @POST("user/auth")
    Observable<UserResponseModel> login(@Query("username") String username,
                                          @Query("password") String password,
                                          @Query("device_token") String deviceToken);

    @GET("logout")
    Observable<ActionResponseModel> logout();
//?username=antipovaleksandr@ukr.net&password=123456&first=Alexander&last=Antipov&phone=0932686097
    @POST("user/register")
    Observable<ActionResponseModel> register(@Query("username") String username,
                                             @Query("email") String email,
                                             @Query("password") String password,
                                             @Query("first") String first,
                                             @Query("last") String last,
                                             @Query("phone") String phone);

    @GET("booking/get/by")
    Observable<BookingResponseModel> getByKey(@Header("Authentication") String cookies,
                                              @Query("key") String key, @Query("page") Integer page);

    @GET("booking/get/notification")
    Observable<ActionResponseModel> getNotifications(@Header("Authentication") String token);

    @POST("user/update-device")
    Observable<ActionResponseModel> updateDeviceToken(@Header("Authentication") String token,
                                                      @Query("token") String deviceToken);

    @POST("user/update-user")
    Observable<ActionResponseModel> updateUser(@Header("Authentication") String cookies,
                                             @Query("email") String email,
                                             @Query("first") String first,
                                             @Query("last") String last,
                                             @Query("phone") String phone);


    @POST("booking/set/notification")
    Observable<ActionResponseModel> sendNotifyRequest(@Query("name") String name,
                                                      @Query("year") String year,
                                                      @Query("month") String month,
                                                      @Query("day") String day,
                                                      @Query("time") String time,
                                                      @Query("telephone") String phone);
}
