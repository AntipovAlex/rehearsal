package com.antipov.brutaltech.interfaces;

import java.io.Serializable;

/**
 * Created by Antipov on 19.09.2017.
 */

public interface OnBookedSuccessfully extends Serializable {
    void onBookedSuccessfully(int count);
}
