package com.antipov.brutaltech.interfaces;

/**
 * Created by Antipov on 10.09.2017.
 */

public interface OnFragmentShowed {
    void onFragmentShowed();
}
